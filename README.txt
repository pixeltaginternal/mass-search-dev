PixelTag Mass Search Protocol

Leverages the integration with PixelTagAdmin to allow the creation and insertion of multiple pixel onto a single merge field. To be used with mass marketing tools (Eloqua, Marketo, Pardot).