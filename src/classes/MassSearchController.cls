public class MassSearchController{

	public String AccountList { get; set; }
	public String newInfo {get;set;}
	public String objectTee {get;set;}

	private List<sObject> contactList;
    
    //Subclass : Wrapper Class 
    public class Contactwrap {
        //Static Variables 
        public string id;
        public string name;
        public string Phone;
        public string Company;
        public string email;

        
        //Wrapper  Class Controller
        Contactwrap() {
            Phone = '';
        }
        
    }

    public MassSearchController() {
    	
    }

    //Method to update sObjects with appropriate value. 
    @RemoteAction
    public static String submitRequest(String textToInput, String sqlQuery, String objectType) {
    	try {
        	if (objectType == 'Lead') {
        		
        		String sql = '';
        		system.debug(sqlQuery);
        		if (sqlQuery == '') {
        			sql = 'SELECT ID, PixelTags__c FROM ' + objectType + ' LIMIT 50000';
        		} else {
        			sql = 'SELECT ID, PixelTags__c FROM ' + objectType + ' WHERE ' + sqlQuery + ' LIMIT 50000';
        		}

	        	System.debug(sql);
	        	List<Lead>lstacc = Database.query(sql);
	        	for (Lead a: lstacc) {
		            a.PixelTags__c = textToInput;		            
		        }

		        update lstacc;
	    	} else {

	    		String sql2 = '';
        		system.debug(sqlQuery);
        		if (sqlQuery == '') {
        			sql2 = 'SELECT ID, PixelTags__c FROM ' + objectType + ' LIMIT 50000';
        		} else {
        			sql2 = 'SELECT ID, PixelTags__c FROM ' + objectType + ' WHERE ' + sqlQuery + ' LIMIT 50000';
        		}
	    		System.debug(sql2);
	    		List <Contact> lstcont = Database.query(sql2);
	    		for (Contact a: lstcont) {
		        	a.PixelTags__c = textToInput;		        	
		        }

		        update lstcont;
	    	}
        
	        return 'true';
        } catch (Exception e) {
        	return 'false';
        }
    }

    //Method to bring the list of Account and Serialize Wrapper Object as JSON
    @RemoteAction
    public static String get2stAccount(String objectType) {
        List <Contactwrap> lstwrap = new List < Contactwrap > ();
        system.debug(objectType);
        if (objectType == 'Lead') {
        	String sql = 'SELECT ID, Name, Phone, Company, Email FROM Lead LIMIT 25';
        	List<Lead>lstacc = [SELECT ID, Name, Phone, Company, Email FROM Lead LIMIT 25];
        	for (Lead a: lstacc) {
	            Contactwrap awrap = new Contactwrap();
	            awrap.id = a.id;
	            awrap.name = a.name;
	            awrap.email = a.email;
	            if (a.Phone != null) {
	                awrap.Phone = a.Phone;
	            }
	            if (a.Company != null) {
	            	awrap.Company = a.Company;
	            }
	            lstwrap.add(awrap);
	        }
    	} else {
    		String sql2 = 'SELECT ID, Name, Phone, Account.Name, Email FROM Contact LIMIT 25';
    		List <Contact> lstcont = [SELECT ID, Name, Phone, Account.Name, Email FROM Contact LIMIT 25];
	        for (Contact a: lstcont) {
	            Contactwrap awrap = new Contactwrap();
	            awrap.id = a.id;
	            awrap.name = a.name;
	            awrap.email = a.email;
	            if (a.Phone != null) {
	                awrap.Phone = a.Phone;
	            }
	            if (a.Account != null) {
	            	awrap.Company = a.Account.Name;
	            }
	            lstwrap.add(awrap);
	        }
    	}
        system.debug(JSON.serialize(lstwrap));
        return JSON.serialize(lstwrap);
     }

     //Method to bring the list of Account and Serialize Wrapper Object as JSON

    @RemoteAction
    public static String getNewList(String sqlQuery, String objectType) {
        List <Contactwrap> lstwrap = new List < Contactwrap > ();
        
        try {
        	if (objectType == 'Lead') {
        		String sql = '';
        		system.debug(sqlQuery);
        		if (sqlQuery == '') {
        			sql = 'SELECT ID, Name, Phone, Company, Email FROM ' + objectType + ' LIMIT 25';
        		} else {
        			sql = 'SELECT ID, Name, Phone, Company, Email FROM ' + objectType + ' WHERE ' + sqlQuery + ' LIMIT 25';
        		}

	        	System.debug(sql);
	        	List<Lead>lstacc = Database.query(sql);
	        	
	        	for (Lead a: lstacc) {
		            Contactwrap awrap = new Contactwrap();
		            awrap.id = a.id;
		            awrap.name = a.name;
		            awrap.email = a.email;
		            if (a.Phone != null) {
		                awrap.Phone = a.Phone;
		            }
		            if (a.Company != null) {
		            	awrap.Company = a.Company;
		            }
		            lstwrap.add(awrap);
		        }
	    	} else {
	    		String sql2 = '';
        		system.debug(sqlQuery);
        		if (sqlQuery == '') {
        			sql2 = 'SELECT ID, Name, Phone, Account.Name, Email FROM ' + objectType + ' LIMIT 25';
        		} else {
        			sql2 = 'SELECT ID, Name, Phone, Account.Name, Email FROM ' + objectType + ' WHERE ' + sqlQuery + ' LIMIT 25';
        		}
	    		System.debug(sql2);
	    		List <Contact> lstcont = Database.query(sql2);
	    		
	    		for (Contact a: lstcont) {
		            Contactwrap awrap = new Contactwrap();
		            awrap.id = a.id;
		            awrap.name = a.name;
		            awrap.email = a.email;
		            if (a.Phone != null) {
		                awrap.Phone = a.Phone;
		            }
		            if (a.Account != null) {
		            	awrap.Company = a.Account.Name;
		            }
		            lstwrap.add(awrap);
		        }
	    	}
        
	        return JSON.serialize(lstwrap);
        } catch (Exception e) {
        	return '{error: "invalid_SQL"}';
        }
        
     }

	@RemoteAction
    public static String getCampaignsData() {

        try {
            HttpRequest req = new HttpRequest();
            //TODO: make endpoint custom setting
            String orgid = UserInfo.getOrganizationId();
            String fullUrl = 'http://pixeltagadmin.herokuapp.com/api/sf/campaigns/' + orgid;
            req.setEndpoint(fullUrl);
            req.setMethod('GET');
            
            Http http = new Http();
            HTTPResponse res = http.send(req);

            /*return '{error: "egetMessage", stack: "egetStackTraceString"}';*/
            //TODO: just return the connections

            String parse2 = res.getBody();
            String parse3 = parse2.unescapeHtml4();
            parse3.replace('&quot;', '\"');
            system.debug(parse3);
            return parse3;
        }
        catch( Exception e ) {
            System.debug(e);
            return '{error:'+e.getMessage()+', stack: '+e.getStackTraceString()+'}';
        }
    }
}