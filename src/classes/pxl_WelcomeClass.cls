public with sharing class pxl_WelcomeClass{

    
    public Integer noEmails {get;set;}


    boolean isInputTextDisabled = false;

    public boolean getIsInputTextDisabled() {
      return isInputTextDisabled;
    }

    public pxl_WelcomeClass() {
        /*
        String str = System.Label.PixelTag_Emails;
        Integer myInt = Integer.valueOf(System.Label.PixelTag_Emails);
        noEmails = myInt;
        */
        try {
            pxl_EmailCount__c newCount = [SELECT ID, Name, Count__c FROM pxl_EmailCount__c WHERE Name='pxlNumber' LIMIT  1];
            noEmails = Integer.valueOf(newCount.Count__c);

        } 
        catch  (Exception e) {
            noEmails = 0;
        }
        
    }


}