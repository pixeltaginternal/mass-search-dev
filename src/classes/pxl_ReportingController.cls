global with sharing class pxl_ReportingController {

    
    public Integer noEmails {get;set;}


    boolean isInputTextDisabled = false;

    public boolean getIsInputTextDisabled() {
      return isInputTextDisabled;
    }

    public pxl_ReportingController() {
        /*
        String str = System.Label.PixelTag_Emails;
        Integer myInt = Integer.valueOf(System.Label.PixelTag_Emails);
        noEmails = myInt;
        */
        try {
            pxl_EmailCount__c newCount = [SELECT ID, Name, Count__c FROM pxl_EmailCount__c WHERE Name='pxlNumber' LIMIT  1];
            noEmails = Integer.valueOf(newCount.Count__c);

        } 
        catch  (Exception e) {
            noEmails = 0;
        }
        
    }

    //Campaign Information Section
    @RemoteAction
    global static String getCampaignsData() {

        try {
            HttpRequest req = new HttpRequest();
            //TODO: make endpoint custom setting
            String orgid = UserInfo.getOrganizationId();
            String fullUrl = 'http://pixeltagadmin.herokuapp.com/api/sf/campaigns/' + orgid;
            req.setEndpoint(fullUrl);
            req.setMethod('GET');
            
            Http http = new Http();
            HTTPResponse res = http.send(req);

            /*return '{error: "egetMessage", stack: "egetStackTraceString"}';*/
            //TODO: just return the connections
            return res.getBody();
        }
        catch( Exception e ) {
            System.debug(e);
            return '{error:'+e.getMessage()+', stack: '+e.getStackTraceString()+'}';
        }
    }


}