@isTest(SeeAllData=true)
private class pxl_TestWelcomeClass{

	private static testMethod void testWelcomeClass() {
		User u = userCreator();

		System.runAs(u) {
			pxl_WelcomeClass wc = new pxl_WelcomeClass();

			System.assert(wc != null);

			Boolean giitd = wc.getIsInputTextDisabled();

			System.assert(giitd == false);

		}
	}


	public static User userCreator() {
		Profile pfiles = [SELECT ID, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];

		User u = new User();
		u.ProfileId = pfiles.id;
		u.FirstName = 'test';
		u.LastName = 'test';
		u.Email = 'test@getpixeltag.com';
		u.Username = 'test.test@getpixeltag.com';
		u.Alias = 'tmethods';
		u.LocaleSidKey = 'en_US';
		u.LANGUAGELOCALEKEY = 'en_US';
		u.EMAILENCODINGKEY = 'UTF-8';
		u.TIMEZONESIDKEY = 'America/Chicago';
		//u.CURRENCYISOCODE = 'USD';

		insert u;

		User u2 = [SELECT ID, Name, Email, Username FROM User WHERE Username =:u.Username LIMIT 1];
		return u2;

	}
}