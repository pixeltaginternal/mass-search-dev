@isTest(SeeAllData=true)
private class pxl_TestReportingController{
	static testMethod void reportingControllerTest() {
		User user1 = userCreator();

		System.runAs(user1) {
			String campData = pxl_ReportingController.getCampaignsData();

			System.assert(campData != null);
		}
		
	}

	static testMethod void reportingControllerNoCountTest() {
		User user1 = userCreator();

		System.runAs(user1) {
			try {
				pxl_EmailCount__c newCount = [SELECT ID, Name, Count__c FROM pxl_EmailCount__c WHERE Name='pxlNumber' LIMIT  1];
				delete newCount;
			} catch (Exception e) {

			}
			pxl_ReportingController ptr = new pxl_ReportingController();

			System.assert(ptr.noEmails == 0);
			String campData = pxl_ReportingController.getCampaignsData();

			System.assert(campData != null);
		}
		
	}

	static testMethod void reportingGetInputTest() {
		User user1 = userCreator();

		System.runAs(user1) {

			pxl_ReportingController ptr = new pxl_ReportingController();
			
			Boolean itd = ptr.getIsInputTextDisabled();

			System.assert(itd == false);
		}
		
	}

	public static User userCreator() {
		Profile pfiles = [SELECT ID, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];

		User u = new User();
		u.ProfileId = pfiles.id;
		u.FirstName = 'test';
		u.LastName = 'test';
		u.Email = 'test@getpixeltag.com';
		u.Username = 'test.test@getpixeltag.com';
		u.Alias = 'tmethods';
		u.LocaleSidKey = 'en_US';
		u.LANGUAGELOCALEKEY = 'en_US';
		u.EMAILENCODINGKEY = 'UTF-8';
		u.TIMEZONESIDKEY = 'America/Chicago';
		//u.CURRENCYISOCODE = 'USD';

		insert u;

		User u2 = [SELECT ID, Name, Email, Username FROM User WHERE Username =:u.Username LIMIT 1];
		return u2;

	}
}